======================
 msp430flasher-fedora
======================

Specfile for packaging the TI MSP430Flasher utility.

See the official `TI`_ page for more details.

RPMs available at the `msp430flasher copr repository`_.

.. _TI: http://processors.wiki.ti.com/index.php/MSP430_Flasher_-_Command_Line_Programmer
.. _msp430flasher copr repository: https://copr.fedoraproject.org/coprs/nielsenb/msp430flasher/
